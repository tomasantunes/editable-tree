# editable-tree
Editable Tree

An utility to write down nested text fields with an extra panel for URLs.

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Y8Y2M1UI)

## Testing

npm install

npm start

## Building

electron-packager . --arch=x64 --platform=win32 --icon=./img/icon.ico
